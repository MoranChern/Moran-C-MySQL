#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<Windows.h>
#include<mysql.h>
#pragma comment(lib, "libmysql.lib")

const char *host = "47.96.27.193";
const char *user = "root";
const char *passwd = "csrwgs001";
const char *db = "yyz";
unsigned int port = 3306;
const char *unix_socket = NULL;
unsigned long client_flag = 0; 

MYSQL_RES *result; 
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
// short if_too_long(char* a){
//     if (strlen(a)>1000){
//         printf("现已输入的参数过多或过长导致命令长度出限制，请重新操作并且避免使命令过长（不超过1000字符）！\n");
//         return 1;
//     }
//     else{
//         return 0;
//     }
// }
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
void _refresh(void);
void _select_all(void);
int _insert();
int _del(char std_id[46]);
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
int main(void){
    _refresh();
    for (;;)
    {
        short input;
        printf("全表查询请输入1\n条件查询请输入2\n增加数据请输入3\n删除数据请输入4\n重新播报请输入0\n请输入：");
        scanf("%1d", &input);
        fflush(stdin);
        switch (input)
        {
        case 0:
            break;
        case 1:
            _select_all();
            break;
        case 2:

            break;
        case 3:
            _insert();
            break;
        case 4:
            _del("");
            break;
        case 9:
            printf("感谢使用！");
            return 0;
        }
    }
}
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
void _refresh(void){
    MYSQL mysql, *sock;

    const char *i_query = "SELECT * FROM `userinfo`";


    mysql_init(&mysql); //连接之前必须使用这个函数来初始化

    if ((sock = mysql_real_connect(&mysql, host, user, passwd, db, port, unix_socket, client_flag)) == NULL) //连接MySQL
    {
        printf("fail to connect mysql \n");
        fprintf(stderr, " %s\n", mysql_error(&mysql));
    }

    if ( mysql_query(&mysql, i_query) != 0 )
    {
        fprintf(stderr, "fail to query!\n");
    }
    else{
        result = mysql_store_result(&mysql);
    }
    mysql_close(sock); //关闭连接    
}
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
void _select_all(void){
    MYSQL_ROW row;
    printf("=============================================================\n||    id  ||    std_id  ||  std_name  ||   std_pwd  || auth||\n=============================================================\n");
    while ((row = mysql_fetch_row(result)) != NULL)
    {
        if (*row[5] == 1)
        {
            printf("||  %4s  ||", row[0]);
            printf("  %8s  ||", row[1]);
            printf("  %8s  ||", row[2]);
            printf("  %8s  ||", row[3]);
            printf("  %1s  ||\n", row[4]);
        }
    }
    printf("=============================================================\n");
}
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
int _insert(){
    char std_id[46],std_name[46],std_pwd[46];
    int auth;

    printf("请注意，学（工）号字段、姓名字段、密码字段限制均为45字符，超出会被截断！\n输入学（工）号，注意避免重用：");
    scanf("%s", &std_id);

    printf("输入姓名：");
    scanf("%s", &std_name);

    printf("输入密码：");
    scanf("%s", &std_pwd);

    printf("如果是管理员请输入1，否则请输入0：");
    scanf("%d", &auth);
    if (auth != 1)
    {
        auth = 0;
        }
    

    int ifdel=0;
    MYSQL_ROW row;
    while ( (row = mysql_fetch_row(result)) != NULL ){
        if(row[1]==std_id&&*row[5]==1){
            int tmp;
            printf("你输入的学（工）号已经存在！输入1覆盖数据，输入0取消插入：");
            scanf("%d", &tmp);
            if(tmp==1){
                ifdel = 1;
                break;
            }
            else{
                return 0;
            }
        }
    }
    if(ifdel==1){_del(row[1]);}

    char cmd[300]="";
    // sprintf(cmd, "123");
    sprintf(cmd, "INSERT INTO `userinfo` (`std_id`, `std_name`, `std_pwd`, `auth`, `validity`) VALUES ('%s', '%s', '%s', %d, 1)", std_id, std_name, std_pwd, auth);
    const char *i_query=cmd;

    MYSQL mysql, *sock;
    mysql_init(&mysql); 

    if ((sock = mysql_real_connect(&mysql, host, user, passwd, db, port, unix_socket, client_flag)) == NULL) //连接MySQL
    {
        printf("fail to connect mysql \n");
        fprintf(stderr, " %s\n", mysql_error(&mysql));
    }

    if ( mysql_query(&mysql, i_query) != 0 ) //如果连接成功，则开始查询 .成功返回0
    {
        fprintf(stderr, "fail to query!\n");
    }
    else{
        fprintf(stderr, "操作已完成！\n");
    }
    mysql_close(sock); //关闭连接
    _refresh();
    return 1;
}
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
int _del(char std_id[46]){

    char id[46] = "";
    for (int i = 0; i < 46;i++){id[i] = std_id[i];}

    if (id == ""){
        printf("请输入要删除的学（工）号");
        scanf("%s", &id);
    }

    int exist = 0;
    MYSQL_ROW row;
    while ( (row = mysql_fetch_row(result)) != NULL ){
        if(row[1]==id&&*row[5]==1){
            exist=1;
            break;
        }
    }
    if(0==exist){
        printf("没有找到需要删除的记录！\n");
        return 0;
    }

    char cmd[200]="0";
    sprintf(cmd, "UPDATE `userinfo`\nSET validity=0\nWHERE `std_id`='%s'", id);
    
    const char *i_query=cmd;

    MYSQL mysql, *sock;
    mysql_init(&mysql); 

    if ((sock = mysql_real_connect(&mysql, host, user, passwd, db, port, unix_socket, client_flag)) == NULL) //连接MySQL
    {
        printf("fail to connect mysql \n");
        fprintf(stderr, " %s\n", mysql_error(&mysql));
        // exit(1);
    }

    if ( mysql_query(&mysql, i_query) != 0 ) //如果连接成功，则开始查询 .成功返回0
    {
        fprintf(stderr, "fail to query!\n");
        // exit(1);
    }
    else{
        fprintf(stderr, "操作已完成！\n");
    }
    mysql_close(sock); //关闭连接
    _refresh();
    return 1;
}