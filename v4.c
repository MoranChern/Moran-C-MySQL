#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<Windows.h>
#include<mysql.h>
#pragma comment(lib, "libmysql.lib")

const char *host = "47.96.27.193";
const char *user = "root";
const char *passwd = "csrwgs001";
const char *db = "yyz";
unsigned int port = 3306;
const char *unix_socket = NULL;
unsigned long client_flag = 0;

int id[1000] = {0}, auth[1000], validity[1000];
char std_id[1000][46],std_name[1000][46],std_pwd[1000][46];
int length;
MYSQL_RES *result; 
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
int num(char []);
void _refresh(void);//更新缓存
int _select_all(void);//返回总行数
int _select(char sel_std_id[46]);//返回选择行数
int _insert();//返回插入行数
int _del(char del_std_id[46]);//返回删除行数
// int _und(char rec_id[46]); //返回恢复行数
int _bak(int);
int _rec(int);
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
int main(void){
    _refresh();
    for (;;)
    {
        short input;
        int tmp = -1;
        printf("全表查询请输入1\n条件查询请输入2\n增加数据请输入3\n删除数据请输入4\n备份请输入5\n恢复请输入6\n刷新缓存请输入8\n退出请输入9\n重新播报请输入0\n请输入：");
        scanf("%1d", &input);
        fflush(stdin);
        switch (input)
        {
        case 0:
            break;
        case 1:
            _select_all();
            break;
        case 2:
            _select("");
            break;
        case 3:
            _insert();
            break;
        case 4:
            _del("");
            break;
        case 5:
            printf("请输入存档点位（0-9），输入-1取消备份：");
            scanf("%d", &tmp);
            if(tmp!=-1){
                _bak(tmp);
            }
            break;
        case 6:
            printf("请输入读取点位（0-9），输入-1取消还原：");
            scanf("%d", &tmp);
            if(tmp!=-1){
                _rec(tmp);
            }
            break;
        case 8:
            _refresh();
            break;
        case 9:
            printf("感谢使用！");
            return 0;
        }
    }
}
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
int num(char a[]){
    int ret = 0, k = 0;
    // printf("string:%s\n", a);
    for (int i = strlen(a) - 1; i >= 0;i--)
    {
        int e = 1;
        for (int j = 1; j <= i; j++)
        {
            e *= 10;
        }
        // printf("i:%d e:%d k:%d\n",i,e,k);
        ret += (a[k]-48) * e;
        k++;
    }
    // printf("num:%d\n\n", ret);
    return ret;
}
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
void _refresh(void){
    
    MYSQL mysql, *sock;
    const char *i_query = "SELECT * FROM `userinfo`";
    MYSQL_RES *result; //保存结果集的
    MYSQL_ROW row; //代表的是结果集中的一行
    mysql_init(&mysql); //连接之前必须使用这个函数来初始化

    if ((sock = mysql_real_connect(&mysql, host, user, passwd, db, port, unix_socket, client_flag)) == NULL) //连接MySQL
    {
        printf("fail to connect mysql \n");
        fprintf(stderr, " %s\n", mysql_error(&mysql));
    }

    if ( mysql_query(&mysql, i_query) != 0 ) //如果连接成功，则开始查询 .成功返回0
    {
        fprintf(stderr, "fail to query!\n");
    }
    else{
        if ( (result = mysql_store_result(&mysql)) == NULL ) //保存查询的结果
        {
            fprintf(stderr, "fail to store result!\n");
        }
        else{
            length = 0;
            while ( (row = mysql_fetch_row(result)) != NULL ){                
                length++;
                id[length-1] = num(row[0]);
                
                for (int k = 0;k<strlen(row[1]);k++){
                    std_id[length - 1][k] = row[1][k];
                }
                std_id[length - 1][strlen(row[1])] = '\0';

                for (int k = 0; k < strlen(row[2]); k++){
                    std_name[length - 1][k] = row[2][k];
                }
                std_name[length - 1][strlen(row[2])] = '\0';

                for (int k = 0;k<strlen(row[3]);k++){
                    std_pwd[length - 1][k] = row[3][k];
                }
                std_pwd[length - 1][strlen(row[3])] = '\0';
            
                auth[length - 1] = num(row[4]);
                validity[length-1] = num(row[5]);

                if(length==1000)
                    break;
            }
        }
    }
    mysql_free_result(result); //释放结果集
    mysql_close(sock); //关闭连接
}
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
int _select_all(void){
    printf("=============================================================\n||    id  ||    std_id  ||  std_name  ||   std_pwd  || auth||\n=============================================================\n");
    for (int i = 0; i < length;i++)
    {
        if(1==validity[i])
        {
            printf("||  %4d  ||  %8s  ||  %8s  ||  %8s  ||  %1d  ||\n",id[i],std_id[i],std_name[i],std_pwd[i],auth[i]);
        }
    }
    printf("=============================================================\n");
    return length;
}
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
int _select(char sel_std_id[46]){

    char s_id[46] = "";
    for (int i = 0; i < 46;i++){s_id[i] = sel_std_id[i];}

    if (sel_std_id == ""){
        printf("请输入要查找的学（工）号：");
        scanf("%s", &s_id);
    }

    int ret = 0;
    printf("=============================================================\n||    id  ||    std_id  ||  std_name  ||   std_pwd  || auth||\n=============================================================\n");
    for (int i = 0; i < length;i++)
    {
        if(1==validity[i]&&strcmp(s_id,std_id[i])==0){
            // printf("s_id:%s std_id:%s\n", s_id, std_id[i]);
            printf("||  %4d  ||  %8s  ||  %8s  ||  %8s  ||  %1d  ||\n",id[i],std_id[i],std_name[i],std_pwd[i],auth[i]);
            ret++;
        }
    }
    printf("=============================================================\n");
    return ret;
}
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
int _insert(){
    char ins_std_id[46],ins_std_name[46],ins_std_pwd[46];
    int ins_auth;

    printf("请注意，学（工）号字段、姓名字段、密码字段限制均为45字符，超出会被截断！\n输入学（工）号，注意避免重用：");
    scanf("%s", &ins_std_id);

    printf("输入姓名：");
    scanf("%s", &ins_std_name);

    printf("输入密码：");
    scanf("%s", &ins_std_pwd);

    printf("如果是管理员请输入1，否则请输入0：");
    scanf("%d", &ins_auth);
    if (ins_auth != 1)
    {
        ins_auth = 0;
    }    

    int ifdel=0;
    for (int i = 0; i < length;i++){
        if(1==validity[i]&&strcmp(ins_std_id,std_id[i])==0){
            printf("你输入的学（工）号已经存在！输入1覆盖数据，输入0取消插入：");
            int input_tmp = 0;
            scanf("%d", &input_tmp);
            if(1==input_tmp){
                _del(ins_std_id);
                break;
            }
            else{
                printf("取消插入！\n\n");
                return 0;
            }
        }
    }    
    if(ifdel==1){}

    char cmd[300]="";
    // sprintf(cmd, "123");
    sprintf(cmd, "INSERT INTO `userinfo` (`std_id`, `std_name`, `std_pwd`, `auth`, `validity`) VALUES ('%s', '%s', '%s', %d, 1)", ins_std_id, ins_std_name, ins_std_pwd, ins_auth);
    const char *i_query=cmd;

    MYSQL mysql, *sock;
    mysql_init(&mysql); 

    if ((sock = mysql_real_connect(&mysql, host, user, passwd, db, port, unix_socket, client_flag)) == NULL) //连接MySQL
    {
        printf("fail to connect mysql \n");
        fprintf(stderr, " %s\n", mysql_error(&mysql));
    }

    if ( mysql_query(&mysql, i_query) != 0 ) //如果连接成功，则开始查询 .成功返回0
    {
        fprintf(stderr, "fail to query!\n");
    }
    else{
        fprintf(stderr, "操作已完成！\n\n");
    }
    mysql_close(sock); //关闭连接
    _refresh();
    return 1;
}
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
int _del(char del_std_id[46]){

    char d_id[46] = "";
    for (int i = 0; i < 46;i++){d_id[i] = del_std_id[i];}

    if (del_std_id == ""){
        printf("请输入要删除的学（工）号：");
        scanf("%s", &d_id);
    }

    int exist = 0;   
    for (int i = 0; i < length;i++){
        if(1==validity[i]&&strcmp(d_id,std_id[i])==0){
            exist = 1;
            break;
        }
    }       


    if(0==exist){
        printf("没有找到需要删除的记录！\n\n");
        return 0;
    }

    char cmd[200]="0";
    sprintf(cmd, "UPDATE `userinfo`\nSET validity=0\nWHERE `std_id`='%s'", d_id);
    
    const char *i_query=cmd;

    MYSQL mysql, *sock;
    mysql_init(&mysql); 

    if ((sock = mysql_real_connect(&mysql, host, user, passwd, db, port, unix_socket, client_flag)) == NULL) //连接MySQL
    {
        printf("fail to connect mysql \n");
        fprintf(stderr, " %s\n", mysql_error(&mysql));
        // exit(1);
    }

    if ( mysql_query(&mysql, i_query) != 0 ) //如果连接成功，则开始查询 .成功返回0
    {
        fprintf(stderr, "fail to query!\n");
        // exit(1);
    }
    else{
        fprintf(stderr, "操作已完成！\n\n");
    }
    mysql_close(sock); //关闭连接
    _refresh();
    return 1;
}
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
int _bak(int a){
    if(0==length){
        return 0;
    }
    a %= 10;
    char filename[9] = "bk0x.txt";
    filename[3] = a + 48;
    
    FILE *fp;
    fp = fopen(filename, "wt");
    if(fp!=NULL){
        fprintf(fp, "INSERT INTO `userinfo` (`std_id`, `std_name`, `std_pwd`, `auth`, `validity`) VALUES\n");
        for (int i = 0; i < length;i++)
        {
            fprintf(fp,"('%s', '%s', '%s', %d, %d)",std_id[i],std_name[i],std_pwd[i],auth[i],validity[i]);
            if(i!=length-1){
                fprintf(fp, ",\n");
            }
        }
        fclose(fp);
    }
    
    return length;
}
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
int _rec(int a){
    a %= 10;
    char filename[9] = "bk0x.txt";
    filename[3] = a + 48;
    int i = 0;
    
    char cmd[10000]="";
    FILE *fp;
    fp = fopen(filename, "rt");

    
    if(fp!=NULL){
        for (char c;!feof(fp);)
        {
            c=fgetc(fp);
            cmd[i++] = c;
        }
        cmd[i-1] = '\0';
        fclose(fp);
    }


    // char kmd[10000] = "INSERT INTO `userinfo` (`std_id`, `std_name`, `std_pwd`, `auth`, `validity`) VALUES\n('123', '123', '123', 1, 0),\n('1231454', 'admin', '32153', 1, 1),\n('1234543', 'test', '123234', 0, 1),\n('123', '321', '234', 0, 0),\n('123', 'asd', 'qwew', 0, 0)";

    // for (int f = 0; f < 10000;f++){
    //     if(cmd[f]!=kmd[f]){
    //         // printf("\n!!!!!!!!!!!!!!!!!!!!!\nfromfile:%c sour:%c,i:%d\n",cmd[f],kmd[f],f);
    //         break;
    //     }
    // }
    // printf("checked cmp:%d\n",strcmp(cmd,kmd));

    printf("您的数据整合命令如下：\n%s\n确认进行恢复吗？输入1确认：", cmd);
    int rec_inp = 0;
    scanf("%d", &rec_inp);
    if(1!=rec_inp){
        printf("您取消了恢复！\n");
        return 0;
    }

    const char *truncate_cmd="truncate `userinfo`";
    const char *i_query=cmd;

    MYSQL mysql, *sock;
    mysql_init(&mysql); 

    if ((sock = mysql_real_connect(&mysql, host, user, passwd, db, port, unix_socket, client_flag)) == NULL) //连接MySQL
    {
        printf("fail to connect mysql \n");
        fprintf(stderr, " %s\n", mysql_error(&mysql));
    }

    if ( mysql_query(&mysql, truncate_cmd) != 0 ) //如果连接成功，则开始查询 .成功返回0
    {
        fprintf(stderr, "擦除失败！\n");
    }
    else{
        fprintf(stderr, "已清除全表数据，即将进行数据写入！\n");
    }


    if ( mysql_query(&mysql, i_query) != 0 ) //如果连接成功，则开始查询 .成功返回0
    {
        fprintf(stderr, "写入数据失败！\n");
    }
    else{
        fprintf(stderr, "已将备份数据写入，操作已完成！\n\n");
    }
    mysql_close(sock); //关闭连接
    _refresh();

    return i - 1;
}